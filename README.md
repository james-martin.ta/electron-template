# Electron Parcel React Boilerplate

Boilerplate code to create basic, production ready Electron app using React, Parcel application bundler.

# Motivation

Base env for every Electron App 

# Installation

 1. `git clone https://gitlab.cloud-grdf.fr/FT5322/electron-boilerplate`
 2.  `npm install`  or `yarn`

# Available Scripts
|commande| effets |
|--|--|
| npm start |start the dev server with live capacities  |
| npm run electron-build |Build the app (.exe, .dmg...)  |


# Ressources and documentation

 - See the medium article [here]([https://medium.com/@yogeshkumarr/production-ready-electron-app-using-react-and-parcel-web-bundler-74dcda63f148](https://medium.com/@yogeshkumarr/production-ready-electron-app-using-react-and-parcel-web-bundler-74dcda63f148)) 
 - And the [original github repo](https://reactjs.org/) as well 